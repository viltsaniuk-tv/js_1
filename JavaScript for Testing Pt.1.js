function task1 (x, y) {
    let result;
    if(x < y)
        result = x + y;
    else if (x > y)
        result = x - y;
    else
        result = x * y; 
    return result;
}

function task2 (a, b) {
    let result = 0;
    for(let i of a)
        result += i;
    for(let i of b)
        result += i;
    return result;
}

function countTrue (a) {
    let result = 0;
    for(let i of a)
        if (i === true)
            result++;
    return result;
}

function doubleFactorial (a) {
    let result = 1;
    let startIndex;
    if(a%2===1)
        startIndex = 3;
    else 
        startIndex = 2;
    for(startIndex; startIndex <= a; startIndex += 2)
         result *= startIndex;
    return result;    
}

function task5_1 (a, b) {
    let result;
    let adjective;
    if(a.age < b.age)
        adjective = "younger than ";
    else if(a.age > b.age)
        adjective = "older than ";
    else
        adjective = "the same age as "
    result = a.name + " is " + adjective + b.name;
    return result;
}

let compareAge = [
    { name: "Anna", age: 15},
    { name: "Bela", age: 25},
    { name: "Bob", age: 20},
    { name: "Kely", age: 45},
    { name: "John", age: 30}
];

function task5_2_a(compareAge){
    compareAge.sort(function (a, b) { return  a.age - b.age; });
    return compareAge;
}

function task5_2_b(compareAge){
    compareAge.sort(function (a, b) { return  b.age - a.age; });
    return compareAge;
}
